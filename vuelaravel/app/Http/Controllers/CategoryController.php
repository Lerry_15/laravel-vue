<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\UploadImage;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::orderBy('created_at', 'desc')->paginate(5);
        return response()->json($category, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(UploadImage $request)
    public function store(UploadImage $request)
    {
        return $category = $this->categoryService->Category($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate(
            [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'mohon inputkan nama anda',
                'name.min' => 'nama anda harus lebih dari 3 huruf',
            ]
        );

        $category->name = $request->name;

        $oldImage = $category->image;
        if ($request->hasFile('image')) {
            $request->validate(
                [
                    'image' => 'required|image|mimes:jpeg,png,jpg'
                ],
                [   
                    'image.required' => 'gambar tidak di inputkan',
                    'image.mimes' => 'format gambar harus jpeg, png, jpg',
                    'image.image' => 'gabar harus berupa gambar'
                ]
            );

            $getname = $request->file('image')->getClientOriginalName();
            $newname = time() . '_' . $getname;
            $path = $request->file('image')->storeAs('public/categories_image', $newname);
            $category->image = $newname;
            
            Storage::delete('public/categories_image/'. $oldImage);
           
        }

        if($category->save()){
            return response()->json($category, 200);
        } else {
            Storage::delete('public/categories_image/'. $newname);
            return response()->json([
                'message' => 'Some error occurred, Please try again!',
                'status_code' => 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->delete()) {
            Storage::delete('public/categories_image/' . $category->image);
            return response()->json([
                'message' => 'Category delete succesfully',
                'status_code' => 200
            ], 200);
        } else {
            return response()->json([
                'message' => 'Some error accurred, please try again',
                'status_code' => 500
            ], 500);
        }
    }
}
