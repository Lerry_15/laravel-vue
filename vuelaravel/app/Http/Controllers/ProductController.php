<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function categories()
    {
        $categories = Category::all();
        return response()->json($categories, 200);
    }
    public function index()
    {
        $product = Product::orderBy('created_at', 'desc')->paginate(5);
        return response()->json($product, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(UploadImage $request)
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'name' => 'required|min:3',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ],
        [
            'name.required' => 'mohon inputkan nama anda',
            'name.min' => 'nama anda harus lebih dari 3 huruf',
            'image.required' => 'mohon inputkan gambar',
            'image.mimes' => 'format gambar harus jpeg, png, jpg',
            'image.image' => 'gabar harus berupa gambar'
        ]);

        $product = new Product;
        $product->category_id = $request->category_id;
        $product->name = $request->name;

        $getname = $request->file('image')->getClientOriginalName();
        $newname = time().'_'.$getname;
        $path = $request->file('image')->storeAs('public/products_image', $newname); 

        $product->image = $newname;

        if($product->save())
        {
            return response()->json($product, 200);
        } else {
            return response()->json([
                'message' => 'Some error occurred, please try again',
                'status_code' => 5000
            ],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate(
            [
                'name' => 'required|min:3',
            ],
            [
                'name.required' => 'mohon inputkan nama anda',
                'name.min' => 'nama anda harus lebih dari 3 huruf',
            ]
        );

        $product->name = $request->name;

        $oldImage = $product->image;
        if ($request->hasFile('image')) {
            $request->validate(
                [
                    'image' => 'required|image|mimes:jpeg,png,jpg'
                ],
                [   
                    'image.required' => 'gambar tidak di inputkan',
                    'image.mimes' => 'format gambar harus jpeg, png, jpg',
                    'image.image' => 'gabar harus berupa gambar'
                ]
            );

            $getname = $request->file('image')->getClientOriginalName();
            $newname = time() . '_' . $getname;
            $path = $request->file('image')->storeAs('public/products_image', $newname);
            $product->image = $newname;
            
            Storage::delete('public/products_image/'. $oldImage);
           
        }

        if($product->save()){
            return response()->json($product, 200);
        } else {
            Storage::delete('public/products_image/'. $newname);
            return response()->json([
                'message' => 'Some error occurred, Please try again!',
                'status_code' => 500
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product->delete()) {
            Storage::delete('public/products_image/' . $product->image);
            return response()->json([
                'message' => 'Product delete succesfully',
                'status_code' => 200
            ], 200);
        } else {
            return response()->json([
                'message' => 'Some error accurred, please try again',
                'status_code' => 500
            ], 500);
        }
    }
}
