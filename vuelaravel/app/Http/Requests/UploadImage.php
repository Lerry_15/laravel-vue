<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadImage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'mohon inputkan nama anda',
            'name.min' => 'nama anda harus lebih dari 3 huruf',
            'image.required' => 'mohon inputkan gambar',
            'image.mimes' => 'format gambar harus jpeg, png, jpg',
            'image.image' => 'gabar harus berupa gambar'
        ];
    }
}
