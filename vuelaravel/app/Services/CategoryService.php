<?php


namespace App\Services;

use App\Category;
use Illuminate\Http\Request;

class CategoryService 
{
    public function Category(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|min:3',
        //     'image' => 'required|image|mimes:jpeg,png,jpg'
        // ],
        // [
        //     'name.required' => 'mohon inputkan nama anda',
        //     'name.min' => 'nama anda harus lebih dari 3 huruf',
        //     'image.required' => 'mohon inputkan gambar',
        //     'image.mimes' => 'format gambar harus jpeg, png, jpg',
        //     'image.image' => 'gabar harus berupa gambar'
        // ]);

        $category = new Category;
        $category->name = $request->name;
        $getname = $request->file('image')->getClientOriginalName();
        $newname = time().'_'.$getname;
        $path = $request->file('image')->storeAs('public/categories_image', $newname); 

        $category->image = $newname;

        if($category->save())
        {
            return response()->json($category, 200);
        } else {
            return response()->json([
                'message' => 'Some error occurred, please try again',
                'status_code' => 5000
            ],500);
        }
        dd($request->all());
    }

}